package com.lagou.demo1.servlet;

import com.lagou.HttpServlet;
import com.lagou.Request;
import com.lagou.Response;
import com.lagou.util.HttpProtocolUtil;

import java.io.IOException;

/**
 * @author YueNing
 */
public class Demo1Servlet extends HttpServlet {

    /**
     * get请求
     * @param request
     * @param response
     */
    @Override
    public void doGet(Request request, Response response) {
        doPost(request, response);
    }

    /**
     * post请求
     * @param request
     * @param response
     */
    @Override
    public void doPost(Request request, Response response) {
        String content = "<h1>demo1 servlet</h1>";
        try {
            response.output(HttpProtocolUtil.getHttpHeader200(content.getBytes().length) + content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() throws Exception {

    }

    @Override
    public void destroy() throws Exception {

    }
}
