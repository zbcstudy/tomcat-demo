package com.lagou;

import com.lagou.util.HttpProtocolUtil;

import java.io.IOException;

/**
 * @author aaron
 * @since 2021/8/23
 */
public class LagouServlet extends HttpServlet{

    @Override
    public void doGet(Request request, Response response) throws IOException {
        try {
            Thread.sleep(5000l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String content = "<h1>lagou servlet get</h1>";
        response.output(HttpProtocolUtil.getHttpHeader200(content.getBytes().length) + content);

    }

    @Override
    public void doPost(Request request, Response response) {

    }

    @Override
    public void init() throws Exception {

    }

    @Override
    public void destroy() throws Exception {

    }
}
