package com.lagou;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author aaron
 * @since 2021/8/23
 */
public class Request {

    /**
     * 请求方式 GET post
     */
    private String method;

    private String url;

    /**
     * 其他属性 从输入流中解析出来
     */
    private InputStream inputStream;


    public Request(InputStream inputStream) throws IOException {
        this.inputStream = inputStream;
        int count = 0;
        while (count == 0) {
            count = inputStream.available();
        }
        byte[] bytes = new byte[count];
        inputStream.read(bytes);
        String inputStr = new String(bytes);
        String firstLine = inputStr.split("\\n")[0];
        String[] strings = firstLine.split(" ");
        this.method = strings[0];
        this.url = strings[1];
        System.out.println("=====>method: " + method);
        System.out.println("=====>url: " + url);

    }

    public Request() {
    }


    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }
}
