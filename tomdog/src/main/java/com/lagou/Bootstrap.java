package com.lagou;

import cn.hutool.core.util.ZipUtil;
import com.lagou.util.BootStrapClassloader;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * 启动类
 *
 * @author aaron
 * @since 2021/8/23
 */
public class Bootstrap {

    /**
     * 启动监听端口
     */
    private int port = 8088;

    // app存储路径
    private static String appBasePath;

    private Map<String, HttpServlet> servletMap = new HashMap<>();

    /** 线程池 */
    private static ThreadPoolExecutor executor;

    /**
     * 初始化操作： 监听
     */
    private void start() throws IOException {
        // 加载服务配置
        loadServerConfig();

        // 解压war包
        unzipWar();

        initWebXml();

        //初始化线程池
        initThreadPool();

        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("mini cat start at:" + port);

        while (true) {
            Socket socket = serverSocket.accept();
            executor.execute(new RequestProcessor(socket, servletMap));
        }

        // 多线程
        /*while (true) {
            Socket socket = serverSocket.accept();
            RequestProcessor requestProcessor = new RequestProcessor(socket, servletMap);
            requestProcessor.start();
        }*/


//        while (true) {
//            Socket socket = serverSocket.accept();
//            // 接收到请求 获取输出流
//            OutputStream outputStream = socket.getOutputStream();
//            String message = "hello mini cat";
//            String data = HttpProtocolUtil.getHttpHeader200(message.getBytes().length) + message;
//            outputStream.write(data.getBytes());
//            socket.close();
//        }
        // 2.0 静态资源
//        while (true) {
//            Socket socket = serverSocket.accept();
//            InputStream inputStream = socket.getInputStream();
//
//            //封装request对象
//            Request request = new Request(inputStream);
//            Response response = new Response(socket.getOutputStream());
//            response.outputHtml(request.getUrl());
//            socket.close();
//        }

        // 3.0 静态资源

    }

    /**
     * 初始化web.xml配置 解析servlet配置
     * 按App name
     */
    private void initWebXml() {
        File file = new File(appBasePath);

        // 文件夹过滤
        File[] apps = file.listFiles(File::isDirectory);

        if (apps != null && apps.length > 0) {
            for (File app : apps) {
                // 项目名
                String appName = app.getName();

                // 文件存储路径
                String appBasePath = app.getPath() + File.separator + "WEB-INF" + File.separator;

                String webXmlPath =  appBasePath + "web.xml";
                String appClassPath = appBasePath + "classes";
                // 获取应用下的web.xml文件，并进行解析
                try {
                    loadServlet(appName, webXmlPath, appClassPath);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * 加载自定义的server.xml配置
     */
    private void loadServerConfig() {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("server.xml");
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(inputStream);

            // 获取根目录
            Element rootElement = document.getRootElement();

            // 解析端口
            Element connectorElement = (Element) rootElement.selectNodes("//Connector").get(0);
            String portStr = connectorElement.attributeValue("port");
            if (!portStr.equals("") || portStr != null) {
                port = Integer.parseInt(portStr);
            }

            // 解析appBase
            Element hostNode = (Element) rootElement.selectNodes("//Host").get(0);
            appBasePath = hostNode.attribute("appBase").getValue();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    /**
     * 解压war包， 将war包解压成文件夹
     */
    private void unzipWar() {
        File app = new File(appBasePath);

        // 拿到所有的war包
        File[] files = app.listFiles(file -> file.getName().endsWith(".war"));
        if (files == null || files.length == 0) {
            System.out.println("没有任何的war包进行部署");
            return;
        }
        for (File file : files) {
            String newFilePath = file.getAbsolutePath().replaceAll(".war", "");
            ZipUtil.unzip(file, new File(newFilePath));
        }
    }

    /**
     * 初始化线程池
     */
    private void initThreadPool() {
        int corePoolSize = 10;
        int maximumPoolSize =50;
        long keepAliveTime = 100L;
        TimeUnit unit = TimeUnit.SECONDS;
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(50);
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();
        executor = new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                unit,
                workQueue,
                threadFactory,
                handler
        );
    }

    /**
     * 加载server.xml 初始化servlet
     * @param appName
     * @param webXmlPath
     * @param appClassPath
     */
    private void loadServlet(String appName,String webXmlPath, String appClassPath) throws FileNotFoundException {
//        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(webXmlPath);

        BootStrapClassloader bootStrapClassloader = new BootStrapClassloader(appClassPath);

        InputStream inputStream = new FileInputStream(webXmlPath);
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(inputStream);
            Element rootElement = document.getRootElement();

            List<Element> selectNodes = rootElement.selectNodes("//servlet");
            for (int i = 0; i < selectNodes.size(); i++) {
                Element element = selectNodes.get(i);
                Element servletNameElement = (Element) element.selectSingleNode("servlet-name");
                String servletName = servletNameElement.getStringValue();
                // 获取servletClass
                Element servletClassElement = (Element) element.selectSingleNode("servlet-class");
                String servletClass = servletClassElement.getStringValue();

                // 根据servletName的值找到urlPattern
                Element servletMapping = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
                String urlPattern = servletMapping.selectSingleNode("url-pattern").getStringValue();

                String mappingPath = "/" + appName + urlPattern;

                servletMap.put(mappingPath, (HttpServlet) bootStrapClassloader.findClass(servletClass).newInstance());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 启动入口
     *
     * @param args -
     */
    public static void main(String[] args) throws IOException {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
