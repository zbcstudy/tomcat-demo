package com.lagou;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.Map;

/**
 * @author aaron
 * @since 2021/8/24
 */
public class RequestProcessor extends Thread {

    private Socket socket;

    private Map<String, HttpServlet> servletMap;

    public RequestProcessor(Socket socket, Map<String, HttpServlet> servletMap) {
        this.socket = socket;
        this.servletMap = servletMap;
    }

    @Override
    public void run() {
        InputStream inputStream = null;
        try {
            inputStream = socket.getInputStream();
            //封装request对象
            Request request = new Request(inputStream);
            Response response = new Response(socket.getOutputStream());

            HttpServlet httpServlet = servletMap.get(request.getUrl());
            if (httpServlet == null) {
                // 静态资源处理
                response.outputHtml(request.getUrl());
            } else {
                // 动态资源处理
                httpServlet.service(request, response);
            }
            socket.close();
        } catch (IOException e) {
            System.out.println("service 处理异常" + e);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
