package com.lagou.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author aaron
 * @since 2021/8/23
 */
public class StaticResourceUtil {

    /**
     * 获取静态资源绝对路径
     * @param path -
     * @return -
     */
    public static String getAbsolutePath(String path) {
        String absolutePath = StaticResourceUtil.class.getResource("/").getPath();
        return absolutePath.replaceAll("\\\\", "/") + path;
    }


    /**
     * 读取静态资源输入流 通过输出流输出
     * @param inputStream -
     * @param outputStream -
     */
    public static void outputStaticResource(FileInputStream inputStream, OutputStream outputStream) throws IOException {
        int count = 0;
        while (count == 0) {
            count = inputStream.available();
        }
        int resourceSize = count;
        // http请求头
        outputStream.write(HttpProtocolUtil.getHttpHeader200(resourceSize).getBytes());

        // 内容读取
        long written = 0l; //已经读取的内容长度
        int byteSize = 1024; //缓冲长度
        byte[] bytes = new byte[byteSize];

        while (written < resourceSize) {
            // 一个字节数组就可以处理完
            if (written + byteSize > resourceSize) {
                byteSize = (int) (resourceSize - written);
                bytes = new byte[byteSize];
            }
            inputStream.read(bytes);
            outputStream.write(bytes);

            outputStream.flush();

            written += byteSize;
        }

    }
}
