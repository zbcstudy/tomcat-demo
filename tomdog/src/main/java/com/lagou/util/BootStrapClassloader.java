package com.lagou.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;

/**
 * 加载class文件的类加载器
 * @author aaron
 * @since 2021/8/26
 */
public class BootStrapClassloader extends ClassLoader {

    private String appClassPath;

    public BootStrapClassloader(String appClassPath) {
        this.appClassPath = appClassPath;
    }

    @Override
    public Class<?> findClass(String name) throws ClassNotFoundException {

        try {
            // 获取class文件字节码
            byte[] classByte = getClassByte(name);
            // 加载类
            return defineClass(name, classByte, 0, classByte.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return super.findClass(name);
    }

    /**
     * 获取class文件的二进制字节
     * @param name
     * @return
     * @throws IOException
     */
    private byte[] getClassByte(String name) throws IOException {
        // class二进制文件的全路径
        String classFile = appClassPath + File.separator + name.replace(".", File.separator) + ".class";

        File file = new File(classFile);
        FileInputStream fileInputStream = new FileInputStream(file);
        FileChannel fileChannel = fileInputStream.getChannel();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        WritableByteChannel writableByteChannel = Channels.newChannel(byteArrayOutputStream);
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

        int i;
        while (true) {
            i = fileChannel.read(byteBuffer);
            if (i == 0 || i == -1) {
                break;
            }
            byteBuffer.flip();
            writableByteChannel.write(byteBuffer);
            byteBuffer.clear();
        }

        //关闭资源
        writableByteChannel.close();
        fileChannel.close();
        fileInputStream.close();

        return byteArrayOutputStream.toByteArray();
    }
}
