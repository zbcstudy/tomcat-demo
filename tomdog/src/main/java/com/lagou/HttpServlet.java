package com.lagou;

import java.io.IOException;

/**
 * @author aaron
 * @since 2021/8/23
 */
public abstract class HttpServlet implements Servlet{

    public abstract void doGet(Request request, Response response) throws IOException;


    public abstract void doPost(Request request, Response response);

    @Override
    public void service(Request request, Response response) throws Exception {
        if ("get".equalsIgnoreCase(request.getMethod())) {
            doGet(request, response);
        }else {
            doPost(request, response);
        }
    }
}
