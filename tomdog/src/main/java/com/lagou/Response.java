package com.lagou;

import com.lagou.util.HttpProtocolUtil;
import com.lagou.util.StaticResourceUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 相应对象
 * 核心方法： 输出静态资源
 *
 * @author aaron
 * @since 2021/8/23
 */
public class Response {

    private OutputStream outputStream;

    public Response(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public Response() {
    }

    /**
     * @param path url 根据url来获取静态资源的绝对路径，进一步根据绝对路径获取静态文件
     *
     */
    public void outputHtml(String path) throws IOException {
        // 获取静态资源绝对路径
        String absoluteResourcePath = StaticResourceUtil.getAbsolutePath(path);

        File file = new File(absoluteResourcePath);
        if (file.exists() && file.isFile()) {
            // 输出静态资源
            StaticResourceUtil.outputStaticResource(new FileInputStream(file), outputStream);
        } else {
            // 404
            outputStream.write(HttpProtocolUtil.getHttpHeader404().getBytes());
        }
    }

    public void output(String content) throws IOException {
        outputStream.write(content.getBytes());

    }

}
