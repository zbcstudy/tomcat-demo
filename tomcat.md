# Tomcat
    本质上是实现了servlet规范的一个servlet容器
    HTTP服务器接受请求、封装成request对象 转换成servletRequest对象
## connector container 两个组件
    connector: 连接器组件 和客户端通信 tcp交互 http容器
    container: 容器 负责业务处理 加载和处理servlet servlet容器
    
## 连接器组件 coyote
    封装了底层网络通信 封装socket处理
    使catalina容器与具体的请求协议和io操作完全解耦
    将socket输入转换成request对象，进一步封装后交由Catalina容器进行处理，处理完成之后Catalina通过
    coyote提供的response对象将结果写入输出流
    coyote负责的是具体协议（应用层）和io（传输层）相关内容

### 支持的协议和io
    支持的协议：
        ajp/http1.1/http2
    支持的io
        NIO  jdk实现的NIO
        NIO2 JDK7+ 实现的NIO2
        APR
        BIO
        默认支持NIO
### coyote内部组件
    Endpoint: 通信端点 通信监听的接口 是具体socket接受和发送器
        是对传输层的抽象 用来实现tcp/ip协议
    Processor：是coyote协议处理接口 用来实现http协议  
        Processor接收来自Endpoint的Socket 读取字节流解析成Tomcat request 和response对象
        通过Adapter将其提交到容器处理 是对应用层协议的抽象
    ProtocolHandler: Coyote协议接口 通过Endpoint和Processor 实现针对具体协议的处理能力
    Adapter：由于协议不同 客户端发过来的请求信息不尽相同 Tomcat定义了自己的request来封装请求信息
        ProtocolHandler接口负责解析请求并生成Tomcat request类。但是这并不是标准的servletRequest
        不能使用Tomcat request作为参数来调用容器。Tomcat设计者引入的解决方案是引入coyoteAdapter
        这是适配器模式的经典应用，连接器调用coyoteAdapter的service方法，传入的是Tomcat request对象
        coyoteAdapter负责将Tomcat request转换成 servletRequest再调用容器
        
## Tomcat容器组件Catalina
    Tomcat的核心 其他模块都是为Catalina提供支撑
    coyote提供链接通信
    Jasper提供jsp引擎
    javaEL 表达式语言
    naming 命名服务
    juli 提供日志服务

    Tomcat在启动的时候就会初始化Catalina实例，Catalina实例通过加载server.xml完成其他实例的创建
    创建并管理server, server创建并管理多个服务，每个服务又可以有多个connector和一个container
    
    Catalina 
        负责解析Tomcat的配置文件（server.xml）以此来创建服务器server组件并进行管理
    Server 
        表示整个Catalina servlet容器及其他组件 负责组装并启动servlet引擎 Tomcat连接器 
        通过实现lifecycle接口 提供了一种优雅启动和关闭整个系统的方式
    service 
        server内部的组件 一个server包含多个service 将多个connector组件绑定到一个container
    container
        容器 负责处理用户的servlet请求并返回对象给web用户
    
## Container组件结构
     Engine: 代表整个Catalina的servlet引擎 用来管理多个虚拟站点 一个service最多只能有一个engine
        一个引擎包含多个host
     Host： 代表一个虚拟主机 或者一个站点 可以给Tomcat配置多个虚拟主机地址 一个虚拟主机下包含多个Context
     Context：表示一个web应用程序 一个web应用包含多个wrapper
     Wrapper：表示一个servlet Wrapper作为容器的最底层 不能包含子容器
     上述组件的配置体现在conf/server.xml
    
# Tomcat源码
## Bootstrap 入口类
    main方法
## init流程
    Bootstrap.init
    -> Bootstrap.load 
    -> Catalina.load 
    -> server.init 
    -> LifecycleBase.init
    -> StandardServer.initInternal
    -> StandardService.StandardService
    -> Connector.initInternal(new CoyoteAdapter)
    -> AbstractProtocol.init
    -> AbstractEndpoint.init -> bindWithCleanup()-> bind()
    -> 选择不同的io实现 NioEndpoint.bind() -> initServerSocket()
## start流程
    Bootstrap.init()
    -> Catalina.start()
    -> LifecycleBase.start() -> startInternal()
    -> StandardServer.startInternal()
    -> StandardService.startInternal() -> engin.start -> executor.start
    -> Connector.startInternal (new CoyoteAdapter)
    -> AbstractProtocol.start
    -> AbstractEndpoint.start
    -> NioEndpoint.startInternal
    -> startAcceptorThread() 端口socket监听
    
## servlet处理流程    
    connector-> engine->host->context->wrapper
    Mapper.class 
    
    Endpoint通信端点 接受socket请求 调用processor进行处理
    processor解析处理socket 封装request
    调用CoyoteAdapter进行路径映射 并将request对象转换成servletRequest
    CoyoteAdapter调用Engine 匹配Host
    调用Host 匹配Context
    调用context 匹配到wrapper
    调用wrapper 得到servlet 构造filterChain
    执行过滤器链和servlet
    
# 类加载机制
## JVM类记载机制
    引导类加载器（BootStrapClassLoader） rt.jar
    扩展类加载器 ExtClassloader
    系统类加载器 AppClassLoader
    
    双亲委派模型   
    
## Tomcat类加载机制
    引导类和扩展类作用不变
    系统类加载器默认加载的是CLASSPATH下的类，但是Tomcat的启动脚本并没有使用该加载器， 而是加载Tomcat启动的类 eg：Bootstrap.jar
    通常在Catalina.bat 和 Catalina.sh中指定 位于CATALINA_HOME/bin 下
    Common通用类加载器 加载Tomcat使用以及应用通用的一下类，位于CATALINA_HOME/bin 下 eg servlet-api.jar
    Catalina classloader 用于加载服务器内部可见类 这些类应用程序不能访问
    Shard classloader 用于加载应用程序共享类 这些类服务器不会依赖
    WebApp classloader 每个应用程序都会有一个独一无二的WebApp classloader 用来加载本应用程序下的 /WEN-INF/classes, /WEB-INF/lib
    
    tomcat8.5 默认改变了严格的双亲委派机制
        首先从 Bootstrap classloader加载指定的类
        未加载到 从 WEB-INF/classes下进行加载
        未加载到 从WEB-INF/lib/*.jar下进行加载
        未加载到 依次从 System Common Shared加载 该步骤使用双亲委派
    
    WebApp classloader最先使用进行加载
    
# https支持
    HTTPS用来加强数据传输安全
    在http的基础上加上 传输加密 和 身份认证 保证传输过程的安全性
    利用SSL/TLS建立全信道，加密数据包
    TLS是传输层加密协议，前身是SSL协议    

## http和HTTPS的区别
    1 HTTP 明文传输，数据都是未加密的，安全性较差，HTTPS（SSL+HTTP） 数据传输过程是加密的，安全性较好。
    2 使用 HTTPS 协议需要到 CA（Certificate Authority，数字证书认证机构） 申请证书，一般免费证书较少，因而需要一定费用。
    证书颁发机构如：Symantec、Comodo、GoDaddy 和 GlobalSign 等。
    3 HTTP 页面响应速度比 HTTPS 快，主要是因为 HTTP 使用 TCP 三次握手建立连接，客户端和服务器需要交换 3 个包，
    而 HTTPS除了 TCP 的三个包，还要加上 ssl 握手需要的 9 个包，所以一共是 12 个包。
    4 http 和 https 使用的是完全不同的连接方式，用的端口也不一样，前者是 80，后者是 443。
    5 HTTPS 其实就是建构在 SSL/TLS 之上的 HTTP 协议，所以，要比较 HTTPS 比 HTTP 要更耗费服务器资源。    

## 三次握手
![三次加密](./05234233-eed6ddcba93c42be8847e98d6da62802.jpg)
    
    第一次握手：客户端尝试连接服务器，向服务器发送 syn 包（同步序列编号Synchronize Sequence Numbers），syn=j，客户端进入 SYN_SEND 状态等待服务器确认
    第二次握手：服务器接收客户端syn包并确认（ack=j+1），同时向客户端发送一个 SYN包（syn=k），即 SYN+ACK 包，此时服务器进入 SYN_RECV 状态
    第三次握手：第三次握手：客户端收到服务器的SYN+ACK包，向服务器发送确认包ACK(ack=k+1），此包发送完毕，客户端和服务器进入ESTABLISHED状态，完成三次握手
    
#Tomcat优化
## jvm虚拟机优化
###优化内存模型
   主要优化内存分配和垃圾回收策略
   内存直接影响服务的运行效率和吞吐量
   jvm垃圾回收机制会不同程度的导致服务运行中断 我们可以选择不同的垃圾回收机制 调整jvm垃圾回收策略
   可以极大的减少垃圾回收次数，提高服务的运行效率
### java 内存模型
    栈: 存储函数运行过程中的一些临时变量
    堆： 用于存储对象
        老年代（old）
        S0
        S1
        Eden： 对象初始化时存放的区域
    本地方法栈：native方法运行栈取
    程序计数器：指向程序运行位置
    方法区（元数据空间）：存储元数据信息（静态方法 常量 类加载器）
    栈、本地方法栈、程序计数器 是线程私有的，堆区和方法区是全局共享的
### 相关参数配置
    -server 启动server,以服务端模式运行 服务端模式建议开启
    -Xms    最小堆内存                 和-Xmx 一起使用 值设置成一直 不用扩容
    -Xmx    最大堆内存                 建议设置为可用内存的80%
    -XX:metaspaceSize   元空间初始值 
    -XX:MaxMetaspaceSize 元空间最大值
    -XX:newRatio        年轻代和老年代的比例 默认为2 不需要修改
    -XX:survivorRatia    eden区和survivor区的比例 取值为正数 默认为8
### 垃圾回收策略
    串行收集器 serial collector
        单进程执行所有的垃圾回收工作 适合单CPU服务器
    并行收集器 parallel collector
        吞吐量收集器
        多线程执行垃圾回收 gc时间变短
    并发收集器 concurrent collector
        工作线程和垃圾回收线程可以同时进行 缩短暂停时间
    CMS收集器 并发标记清除收集器
        并发收集器的一种 采用并发标记清除算法
    G1收集器    
        适用于大容量内存的多核处理器 可以在满足垃圾回收暂停时间目标的同时 以最大可能实现高吞吐量 jdk1.7+ 
    
    -XX:+UseSerialGc 使用串行收集器
    -XX:+UseParallelGC 使用并行收集器 配置了该选项 -XX:+UseParallelOldGC默认启用
    -XX:+UseParNewGC  年轻代采用并行收集器 配置了该选项 -XX:+UseConcMarkSweepGC 自动启用（CMS）
    -XX:ParallelGCThreads 配置并发收集器线程数
    -XX:+UseConcMarkSweepGC 对于老年代 启用CMS垃圾收集器。 当并行收集器无法满足应用的延时需求时 推荐使用CMS或G1收集器
        启动该选项时 -XX:+UseParallelGC 自动启用
    -XX:+UseG1Gc 启动G1收集器 G1收集器是服务器类型的收集器 用于多核大内存机器
        -XX:MaxGCPauseMillis=10 设置在任意1秒的时间内，停顿不得超过10ms 这个参数需要配置
         G1会尽量达成这个目标，它能够反向推算出本次要收集的大体区域，以增量的方式完成收集。
## Tomcat自身优化
###Tomcat线程池调优
    提供一个共享线程池
###Tomcat连接器调优
    tomcat/conf/server.xml
    maxConnections  最大连接数
        当达到最大值时 服务器会接受请求 但是不会进行处理，额外的连接会进行阻塞直到maxConnections低于最大值
    maxThread       最大线程数
        需要考虑到当前服务器的情况
    acceptAccount   最大排队等待数
        当服务器请求达到maxConnections Tomcat会将后序的请求放到队列中进行排队
###禁用AJP连接器
### 调整IO
    BIO->NIO
### 动静分离    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

























    

    

